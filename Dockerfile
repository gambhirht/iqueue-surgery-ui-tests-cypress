FROM cypress/included:8.2.0
RUN mkdir /cypress-docker
WORKDIR /cypress
COPY ./package.json .
COPY ./package-lock.json .
COPY ./cypress.json .
COPY ./cypress ./cypress
RUN npm install
ENTRYPOINT ["npm", "run"]