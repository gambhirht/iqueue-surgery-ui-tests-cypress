class LoginPage {
    navigate(environment) {
        cy.visit(Cypress.env(environment).baseUrl)
    }

    get getEmail() {
        return cy.get('#field-email');
    }

    get getPassword() {
        return cy.get('#field-password');
    }

    get getIqueueLogo() {
        return cy.get('.header');
    }

    get getIqueueTitle() {
        return cy.get('h2');
    }

    get getEmailSectionButton() {
        cy.get('#email-section .btn.btn-submit');
    }

    get getPasswordSectionButton() {
        cy.get('#password-section .btn.btn-submit');
    }

    get getUserAvatar() {
       cy.get('header-main__avatar');
    }

    enterEmail(username) {
        cy.get('#field-email').type(username)
        return this
    }

    enterPassword(password) {
        cy.get('#field-password').type(password)
        return this
    }

    submitEmail() {
        cy.get('#email-section .btn.btn-submit').click();
    }

    submitPassword() {
        cy.get('#password-section .btn.btn-submit').click();
    }
}

export default LoginPage