/// <reference types="cypress" />
import LoginPage from "../pages/auth/login-page.js"
const login = new LoginPage();

describe('Login Page Tests', function () {
    var data;
    before(function () {
        cy.fixture('stage_credentials').then(function (testdata) {
            this.testdata = testdata
            console.log(this.testdata);
            data =this.testdata;
        })

        cy.visit(Cypress.env('stage').baseUrl)
    })

    beforeEach(function () {
      // Nothing to do here for this spec
    })

    after(function() {
        cy.logout();
    })

    it("Login page is formatted properly", function () {
        login.getIqueueTitle.should('have.text',' Welcome to iQueue!  Enter your password.  Part of   Where should we email password reset instructions? ')
    })

    it("Login with valid admin credentials", function () {
        login.enterEmail(data.admin.username);
        login.submitEmail();
        login.enterPassword(data.admin.password);
        login.submitPassword();
        cy.url().should('include', '/dashboard/or-statistics/v2')
        cy.get('.side-nav .ng-star-inserted').should('have.length', 23)
        cy.get('.tiles-container .tile').should('have.length', 14)
    })

    xit("Login with valid surgeon credentials", function () {
        login.enterEmail(data.surgeon.username);
        login.submitEmail();
        login.enterPassword(data.admin.password);
        login.submitPassword();
        cy.url().should('include', '/dashboard/or-statistics/v2')
        cy.get('.side-nav .ng-star-inserted').should('have.length', 23)
        cy.get('.tiles-container .tile').should('have.length', 14)
    })
})