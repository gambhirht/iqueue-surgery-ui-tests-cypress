// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add("login", (environment, role) => {
    cy.url().should('include', 'https://accounts.stage-iqueue.com/auth/login?service=iqueue-surgery')
    cy.get('h2').should('have.text'," Welcome to iQueue!  Enter your password.  Part of   Where should we email password reset instructions? ");

    cy.contains('Welcome to iQueue!')
    cy.contains('© LeanTaaS Inc. 2021')

    let users = Cypress.env(environment).users;
    let filteredUsers = users[role];
    let userName = filteredUsers["username"]
    let password = filteredUsers["password"]
    cy.get('#field-email')
    .type(userName)
    .should('have.value', userName)
    cy.get('#email-section .btn.btn-submit').click()

    cy.get('#field-password')
    .type(password)
    .should('have.value', password)

    cy.get('#password-section .btn.btn-submit').click()
})

Cypress.Commands.add("logout", () => {
    cy.get('.avatar > .circle').click();
    cy.get('.header-main__dropdown-item-link').click();
});

Cypress.Commands.add("api_login", () => {
    cy.get('.avatar > .circle').click();
    cy.get('.header-main__dropdown-item-link').click();
});